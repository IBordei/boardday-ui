import BLoading from '@/components/base/loading';

export default (Vue) => {
    const Mask = Vue.extend(BLoading);

    const append = (el, visible) => {
        el.appendChild(el.mask.$el);
        el.mask.active = visible;
    };

    const destroy = (el) => {
        el.mask.$destroy();
    };

    const getLoadingState = (value) => {
        let visible = false;

        if (typeof value === 'boolean') {
            visible = value;
        }

        if (typeof value === 'object') {
            visible = value.loading;
        }

        return visible;
    };

    return {
        bind: (el, binding) => {
            el.mask = new Mask({
                el: document.createElement('div')
            });

            const scale = binding.value?.scale || 1;
            const background =
                binding.value?.background || 'rgba(255, 255, 255, 0.75)';

            el.mask.scale = scale;
            el.mask.background = background;

            append(el, getLoadingState(binding.value));
        },
        update: (el, binding) => {
            el.mask.active = getLoadingState(binding.value);
        },
        unbind: (el) => {
            destroy(el);
        }
    };
};
