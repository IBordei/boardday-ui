import BLoading from '@/components/base/loading';

export default {
    init: (Vue) => {
        if (Vue.prototype.$isServer) return;
        Vue.prototype.$loading = (propsData) => {
            const Loading = Vue.extend(BLoading);
            const instance = new Loading({ propsData }).$mount();

            if (propsData && propsData.target) {
                if (
                    propsData.target instanceof Vue &&
                    propsData.target.constructor.name === 'VueComponent'
                ) {
                    propsData.target.$el.appendChild(instance.$el);
                } else if (propsData.target instanceof Element) {
                    propsData.target.appendChild(instance.$el);
                } else {
                    return;
                }
            } else {
                document.body.appendChild(instance.$el);
            }

            return instance;
        };
    }
};
