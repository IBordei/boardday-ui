const path = require('path');

const isDev = process.env.NODE_ENV !== 'production';

module.exports = {
    configureWebpack: (config) => {
        config.entry = {
            app: ['./public/main.js']
        };

        config.resolve.alias = {
            ...config.resolve.alias,
            '@': path.resolve(__dirname, './src')
        };
    },
    css: {
        requireModuleExtension: true,
        loaderOptions: {
            css: {
                modules: {
                    localIdentName: isDev
                        ? '[name]__[local]__[hash:base64:4]'
                        : '[hash:base64:4]'
                }
            }
        }
    }
};
