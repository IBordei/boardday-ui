import VueLazyload from 'vue-lazyload';

import BAlert from '@/components/base/alert';
import BAvatar from '@/components/base/avatar';
import BButton from '@/components/base/button';
import BCheckbox from '@/components/base/checkbox';
import BCheckboxButton from '@/components/base/checkbox-button';
import BCollapse from '@/components/base/collapse';
import BCollapseItem from '@/components/base/collapse/collapse-item';
import BDatepicker from '@/components/base/datepicker';
import BDatepickerInput from '@/components/base/datepicker-input';
import BForm from '@/components/base/form';
import BFormItem from '@/components/base/form/form-item';
import BInput from '@/components/base/input';
import BLazyBackground from '@/components/base/lazy-background';
import BListEditor from '@/components/base/list-editor';
import BPoper from '@/components/base/poper';
import BRadio from '@/components/base/radio';
import BRate from '@/components/base/rate';
import BSelect from '@/components/base/select';
import BSwiper from '@/components/base/swiper';
import BSwiperSlide from '@/components/base/swiper/swiper-slide';
import BTabs from '@/components/base/tabs';
import BTabsItem from '@/components/base/tabs/tabs-item';
import BTag from '@/components/base/tag';
import BTooltip from '@/components/base/tooltip';
import BUpload from '@/components/base/upload';
import BUploadAvatar from '@/components/base/upload-avatar';
import BUploadGallery from '@/components/base/upload-gallery';
import BContainer from '@/components/base/container';
import BDropdown from '@/components/base/dropdown';
import BDropdownItem from '@/components/base/dropdown/item';
import BBadge from '@/components/base/badge';
import BNavigation from '@/components/base/navigation';
import BNavigationItem from '@/components/base/navigation/item';
import BResult from '@/components/base/result';
import BTable from '@/components/base/table';
import BTableTh from '@/components/base/table/th';
import BTableTd from '@/components/base/table/td';

/* Functions */
import FnDialog from '@/functions/dialog';
import FnLoading from '@/functions/loading';
import FnNotification from '@/functions/notification';

/* Directives */
import clickOutside from '@/directives/click-outside';
import loadingDirective from '@/directives/loading';

/* Styles */
import '@/assets/styles/index.css';

/* Define plugin */
const components = [
    BAlert,
    BAvatar,
    BButton,
    BCheckbox,
    BCheckboxButton,
    BCollapse,
    BCollapseItem,
    BDatepicker,
    BDatepickerInput,
    BForm,
    BFormItem,
    BInput,
    BLazyBackground,
    BListEditor,
    BPoper,
    BRadio,
    BRate,
    BSelect,
    BSwiper,
    BSwiperSlide,
    BTabs,
    BTabsItem,
    BTag,
    BTooltip,
    BUpload,
    BUploadAvatar,
    BUploadGallery,
    BContainer,
    BDropdown,
    BDropdownItem,
    BBadge,
    BNavigation,
    BNavigationItem,
    BResult,
    BTable,
    BTableTh,
    BTableTd
];

const functions = [FnDialog, FnLoading, FnNotification];

const install = (Vue) => {
    components.forEach((component) => {
        Vue.component(component.name, component);
    });

    if (Vue.prototype.$isServer) return;

    Vue.prototype.$bui = {};

    functions.forEach((fn) => {
        fn.init(Vue);
    });

    Vue.directive('click-outside', clickOutside);
    Vue.directive('loading', loadingDirective(Vue));

    Vue.use(VueLazyload);
};

export default {
    install,
    clickOutside,
    ...components
};
