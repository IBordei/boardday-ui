import FnDialog from './dialog.vue';

export default {
    init: (Vue) => {
        Vue.prototype.$dialog = (propsData) => {
            const Dialog = Vue.extend(FnDialog);
            const instance = new Dialog({ propsData }).$mount();
            document.body.appendChild(instance.$el);
            return instance;
        };
    }
};
