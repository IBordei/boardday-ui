import moment from 'moment';

moment.updateLocale('ru', {
    week: {
        dow: 1
    }
});

export const namesOfDays = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
export const namesOfMonths = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'
];

export const now = moment();

export const generateDatesArray = (
    year = now.get('year'),
    month = now.get('month')
) => {
    const firstDayOfMonth = moment([year, month])
        .startOf('month')
        .startOf('week')
        .set({
            hour: 0,
            minute: 0,
            second: 0,
            milisecond: 0
        });
    const dates = [];
    for (let i = 0; i < 42; i++) {
        const clone = firstDayOfMonth.clone();
        dates.push({
            value: clone._d,
            year: clone.get('year'),
            month: clone.get('month'),
            date: clone.get('date')
        });
        firstDayOfMonth.add(1, 'day');
    }
    return dates;
};

export { moment };
