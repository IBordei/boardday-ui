const clickOutside = {
    bind(el, binding, vnode) {
        let targetDown, targetUp;

        el.onMouseDown = function (event) {
            targetDown = event.target;
        };

        el.onMouseUp = function (event) {
            targetUp = event.target;

            if (
                targetDown === targetUp &&
                targetDown !== el &&
                !el.contains(targetDown)
            ) {
                vnode.context[binding.expression](event);
            } else {
                event.stopPropagation();
            }
        };

        document.addEventListener('mousedown', el.onMouseDown);
        document.addEventListener('mouseup', el.onMouseUp);
    },
    unbind(el) {
        document.removeEventListener('mousedown', el.onMouseDown);
        document.removeEventListener('mouseup', el.onMouseUp);
    }
};

module.exports = clickOutside;
