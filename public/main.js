import Vue from 'vue';
import App from './App.vue';
import VueLazyload from 'vue-lazyload';

import FnDialog from '@/functions/dialog';
import FnLoading from '@/functions/loading';
import FnNotification from '@/functions/notification';

import LoadingDirective from '@/directives/loading';

Vue.directive('loading', LoadingDirective(Vue));

Vue.use(VueLazyload);

import '@/assets/styles/index.css';

const functions = [FnDialog, FnLoading, FnNotification];

functions.forEach((fn) => {
    fn.init(Vue);
});

Vue.config.productionTip = false;

new Vue({
    render: (h) => h(App)
}).$mount('#app');
